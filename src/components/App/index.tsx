import { LocalizationProvider } from '@mui/x-date-pickers';
import React from 'react';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns'
import AppContent from '../AppContent';
import { Provider } from 'react-redux';
import store from '../../store';
import { BrowserRouter } from 'react-router-dom';

const App = () => {
  return (
    <BrowserRouter>
      <Provider store={store}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <AppContent/>
        </LocalizationProvider>
      </Provider>
    </BrowserRouter>
  );
}


export default App;
