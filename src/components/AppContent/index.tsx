import { Navigate, useRoutes } from "react-router-dom"
import InfoPage from "../../pages/InfoPage"
import SearchPage from "../../pages/SearchPage"


const AppContent = () => {

    const routes =  useRoutes([
        {
            path: "/avia",
            children: [
                {
                    path:'',
                    element: <SearchPage src={'/avia/info'}/>
                },
                {
                    path:'info',
                    element: <InfoPage/>
                }
            ],
        },
        {   path:'*',
            element: <Navigate to={'/avia'}/>
        },
    ])

    return (
        <div className="app-content">
            {routes}
        </div>
    )
}

export default AppContent