import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './style.scss'
import { selectBackDate, selectThereDate, selectArrivalDirection, selectDepartureDirection } from '../../store/search/selectors'
import { routeAction } from '../../store/search/actions';
import { Link } from 'react-router-dom';

const SearchButton = ({linkSrc}:{linkSrc: string} ) => {

    const dispatch = useDispatch()
    const backDate = useSelector(selectBackDate);
    const thereDate = useSelector(selectThereDate);
    const arrivalDirection = useSelector(selectArrivalDirection);
    const departureDirection = useSelector(selectDepartureDirection);
    
    let route = 'none';
    let buttonStatus = 'disabled';
    let src;

    useEffect(()=> {
        dispatch(routeAction(route));
    })

    if (arrivalDirection && departureDirection && thereDate && arrivalDirection !== departureDirection) {
        if (backDate) {
            let [day1, month1, year1] = thereDate.split('.')
            let [day2, month2, year2] = backDate.split('.')
            const correctThereDate = new Date(+year1, +month1, +day1)
            const correctBackDate = new Date(+year2, +month2, +day2)
            correctBackDate >= correctThereDate ? route = 'round-trip': route= 'none'
        } else {
            route ='one-way' 
        }
    }    

    if (route === 'none') {
        buttonStatus = 'disabled';
        src = '';
    } else {
        buttonStatus = 'enabled';
        src = linkSrc
    }

    return (
        <Link className={`search-button ${buttonStatus}`} to={src}>
            <p className="search-button__text">Найти билеты</p>
        </Link>
    )
}

export default SearchButton;