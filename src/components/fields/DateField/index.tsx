import './style.scss'

import calendarNonActive from '../../../assets/icons/calendar_non_active.svg'
import calendarActive from '../../../assets/icons/calendar_active.svg'
import DatePicker  from 'react-datepicker';
import React, { ForwardedRef, useEffect, useState } from 'react';
import { addDays, subDays } from 'date-fns';
import { ISearchAction } from '../../../store/search/types';
import { useDispatch, useSelector } from 'react-redux';
import { selectThereDate } from '../../../store/search/selectors';

interface IFieldItem {
    title: string,
    placeholder: string,
    action: (value: string)=> ISearchAction
}

const DateField = ({title, placeholder, action}:IFieldItem) => {

    let lastSymbol='';
    const thereDate = useSelector(selectThereDate);
    const today = new Date();
    const dispatch = useDispatch();
    const [selectDate, setSelectDate] = useState('');

    useEffect(()=> {
        selectDate && dispatch(action(selectDate))
    })

    const showCalendar = (func: () => void) => {
        if (title === 'Туда' || thereDate) return func
        else return () => console.log('Не выбран перелет туда')
    }

    const IconDatePicker = React.forwardRef((props: any, ref: ForwardedRef<HTMLImageElement>) => {
        let img;
        selectDate ? img = calendarActive : img = calendarNonActive;
        return <img src={img} className='field__input__icon' onClick={showCalendar(props.onClick)} ref={ref} alt='calendar'/>
    });

    const calendarToInput = (date: Date) => {
        const checkQuantity = (num: string | number) => {
            if (num < 10) {
                return '0'+num
            } else return num;
        }
        return checkQuantity(date.getDate())+'.'+ (checkQuantity(date.getMonth()+1)) +'.' + date.getUTCFullYear().toString().substring(2,4)
    }

    const checkCorrectDate = (stringDate: string) => {
        if (stringDate.length === 8) {
            const [day, month, year] = stringDate.split('.').map(item => Number(item))
            const inputDate = new Date(year+2000, (month-1), day)
                if (inputDate.getFullYear() === year +2000 && inputDate.getMonth() === month-1 && inputDate.getDate() === day) {
                    if (inputDate >= today) {
                        return inputDate;
                    }
                    else return null;
                } else return null;
        }
    }

    const clearSelectDate = () => {
        dispatch(action(''));
        setSelectDate('');
    }


    const setInputValue = (e: React.ChangeEvent<HTMLInputElement>) => {
        const inputData = e.target.value;
        if(!isNaN(Number(lastSymbol)) || lastSymbol === 'Backspace') {
            if ( (inputData.length === 2 && lastSymbol !=='Backspace') || (inputData.length === 5 && lastSymbol !=='Backspace') ) {
                setSelectDate(inputData + '.')
            } else if (inputData.length <= 8) {
                setSelectDate(inputData)
            } 
        }
    }

    return(
        <div className="field">
            <p className="field__input-title">{title}</p>
            <div className="field__input-wrapper">
            <DatePicker
                includeDateIntervals={[
                    { start: subDays(today, 1), end: addDays(today, 365) },
                ]}
                selected={checkCorrectDate(selectDate)}
                onChange={(selectDate) => selectDate && setSelectDate(calendarToInput(selectDate))}
                customInput={<IconDatePicker />}
            />
                <input 
                    type='text' 
                    className='field__input date' 
                    placeholder={placeholder} 
                    value={selectDate} 
                    onChange={(e)=>setInputValue(e)}
                    onKeyDown={e => lastSymbol = e.key}
                    onBlur={() => !checkCorrectDate(selectDate) && clearSelectDate()} 
                />
            </div>
        </div>
    )
}

export default DateField;