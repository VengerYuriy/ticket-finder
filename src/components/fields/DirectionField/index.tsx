import './style.scss'


import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { ISearchAction } from '../../../store/search/types';
import { airportList } from '../../fixtures/airportList';

interface IFieldItem {
    title: string,
    placeholder: string,
    actionCity: (value: any)=> ISearchAction,
    actionAirport: (value: any)=> ISearchAction
}

const DirectionField = ({title, placeholder = '', actionCity, actionAirport }:IFieldItem) => {
    const dispatch = useDispatch()
    const [isVisible, setVisible] = useState(false);
    const [searchValue, setSearchValue] = useState('');

    const pickFromList = (e: React.MouseEvent<HTMLParagraphElement, MouseEvent>) => {
        const target = e.target as HTMLParagraphElement
        if (target.textContent) {
            setSearchValue(target.textContent)
            setContent(target.textContent)
        }
    }

    const setContent = (value: string) => {
        const matches = airportList.filter(item => item.name.toUpperCase() === value.toUpperCase())
        if (matches.length) {
            dispatch(actionCity(matches[0].name))
            dispatch(actionAirport(matches[0].airport))
        } else {
            setSearchValue(value);
            dispatch(actionCity(''));
            dispatch(actionAirport(''));
        }
        setTimeout(()=> setVisible(false), 150)
    }

    return(
        <div className="field">
            <p className="field__input-title">{title}</p>
            <div className="field__input-wrapper">
                <input type='text' 
                    value={searchValue}
                    placeholder={placeholder} 
                    className='field__input' 
                    onFocus={()=> setVisible(true)} 
                    onBlur={()=>  setContent(searchValue)}
                    onChange={e => setSearchValue(e.target.value)}
                />
            </div>
            <div className={`field__airports-list + ${isVisible ? 'visible' : 'invisible'}`} >
                {airportList.map((item, index)=> {
                    if (item.name.toUpperCase().indexOf(searchValue.toUpperCase()) > -1) {
                        return <p className = "field__airports-list__item" key={index} onClick={e => pickFromList(e)} tabIndex={0}>{item.name}</p>
                    } else return null;
                })}
            </div>
        </div>
    )
}

export default DirectionField;