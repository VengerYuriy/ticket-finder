import './style.scss'
import baggage from '../../../../assets/icons/baggage.svg'
import luggage from '../../../../assets/icons/luggage.svg'

interface IFLightRoute {
    departureCity: string,
    departureAirport: string,
    arrivalCity: string,
    arrivalAirport: string,
    departureDate: string,
    departureTime:string,
    arrivalDate: string,
    arrivalTime: string,
    price: number,
    company: string,
}



const FlightRoute = ({object}:{object: IFLightRoute} ) => {

    const calculateDuration = (time1: string, time2:string) => {
        const [hours1, minutes1] = time1.split(':');
        const [hours2, minutes2] = time2.split(':');
        const duration = Number(hours2)*60 + Number(minutes2) - Number(hours1)*60 - Number(minutes1);
        const minutes = duration % 60;
        const hours = (duration-minutes)/60;
        return `в пути ${hours} ч ${minutes} мин`;
    }

    return (
    <div className="info-card__flight__route">
        <div className="info-card__flight__route__time departure">
            <p className="info-card__flight__route__time__title">{object.departureTime}</p>
            <p className="info-card__flight__route__time__place">{object.departureCity}</p>
            <p className="info-card__flight__route__time__date">{object.departureDate}</p>
        </div>
        <div className="info-card__flight__route__plan">
            <div className="info-card__flight__route__plan__place">
                <p>{object.departureAirport}</p>
                <p>{object.arrivalAirport}</p>
            </div>
            <div className="info-card__flight__route__plan__figure">
                <div className='circle'/>
                <div className='line'/>
                <div className='circle'/>
            </div>
            <p>{calculateDuration(object.departureTime, object.arrivalTime)}</p>
        </div>
        <div className="info-card__flight__route__time arrival">
        <p className="info-card__flight__route__time__title">{object.arrivalTime}</p>
            <p className="info-card__flight__route__time__place">{object.arrivalCity}</p>
            <p className="info-card__flight__route__time__date">{object.arrivalDate}</p>
        </div>
        <div className="info-card__flight__route__baggage__wrapper">
            <div className='info-card__flight__route__baggage__icons'>
                <img src={luggage} alt='luggage'/>
                <img src={baggage} alt='baggage'/>
            </div>
        </div>
    </div>
    )
}
export default FlightRoute;