import './style.scss'

interface IFlightVariant {
    className: string,
    departureTime: string,
    arrivalTime: string,
    handleFunc: () => void
}

const FlightVariant = ({className, arrivalTime, departureTime, handleFunc}: IFlightVariant) => {

    return (
        <div className={`flight-variant__wrapper ${className}`} onClick={handleFunc}>
            <p><strong>{departureTime}</strong> - {arrivalTime}</p>
        </div>
    )
}

export default FlightVariant;