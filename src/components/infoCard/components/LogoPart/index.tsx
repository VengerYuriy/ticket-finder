import './style.scss'

const LogoPart = ({logo}:{logo:string}) => {

    return (
        <div className="info-card__logo__wrapper">
            <span className="info-card__logo__header">Невозвратный</span>
            <div className="info-card__logo__icon__wrapper">
                <img className='info-card__logo__icon' src={logo} alt='logo'/>
                <p className="info-card__logo__description">S7 Airlines</p>
            </div>
        </div>
    )
}

export default LogoPart;