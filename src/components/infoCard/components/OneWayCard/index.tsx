import './style.scss'

import S7 from '../../../../assets/icons/S7.svg'
import FlightVariant from '../FlightVariant'
import LogoPart from '../LogoPart'
import FlightRoute from '../FlightRoute'
import { selectArrivalDirection, selectThereDate, selectDepartureDirection, selectArrivalAirport, selectDepartureAirport, selectBackDate } from '../../../../store/search/selectors'
import { useDispatch, useSelector } from 'react-redux'
import { useEffect, useState } from 'react'
import { backArrivalTimeAction, backDepartureTimeAction, backPriceAction, thereArrivalTimeAction, thereDepartureTimeAction, therePriceAction } from '../../../../store/search/actions'

const OneWayCard = ({direction}: {direction: string}) => {
    const dispatch = useDispatch()
    const departureDirection = useSelector(selectDepartureDirection),
          arrivalDirection = useSelector(selectArrivalDirection),
          thereDate = useSelector(selectThereDate),
          backDate = useSelector(selectBackDate),
          arrivalAirport = useSelector(selectArrivalAirport),
          departureAirport = useSelector(selectDepartureAirport)
          
    const mockFlightInfo = [
        {
            departureCity: direction === 'there' ? departureDirection : arrivalDirection,
            departureAirport: direction === 'there' ? departureAirport : arrivalAirport,
            arrivalCity: direction === 'there' ? arrivalDirection : departureDirection,
            arrivalAirport: direction === 'there' ? arrivalAirport : departureAirport,
            departureDate: direction === 'there' ? thereDate : backDate,
            arrivalDate: direction === 'there' ? thereDate : backDate,
            departureTime:'10:15',
            arrivalTime: '12:05',
            price: 4150,
            company: 'S7'
        },
        {
            departureCity: direction === 'there' ? departureDirection : arrivalDirection,
            departureAirport: direction === 'there' ? departureAirport : arrivalAirport,
            arrivalCity: direction === 'there' ? arrivalDirection : departureDirection,
            arrivalAirport: direction === 'there' ? arrivalAirport : departureAirport,
            departureDate: direction === 'there' ? thereDate : backDate,
            arrivalDate: direction === 'there' ? thereDate : backDate,
            departureTime:'11:25',
            arrivalTime: '14:55',
            price: 4200,
            company: 'S7'
        },
        {
            departureCity: direction === 'there' ? departureDirection : arrivalDirection,
            departureAirport: direction === 'there' ? departureAirport : arrivalAirport,
            arrivalCity: direction === 'there' ? arrivalDirection : departureDirection,
            arrivalAirport: direction === 'there' ? arrivalAirport : departureAirport,
            departureDate: direction === 'there' ? thereDate : backDate,
            arrivalDate: direction === 'there' ? thereDate : backDate,
            departureTime:'15:15',
            arrivalTime: '17:55',
            price: 4250,
            company: 'S7'
        }
    ]

    const [activeRoute, setActiveRoute] = useState(0)

    useEffect(() => {
        if (direction === 'there') {
            dispatch(thereDepartureTimeAction(mockFlightInfo[activeRoute].departureTime));
            dispatch(thereArrivalTimeAction(mockFlightInfo[activeRoute].arrivalTime));
            dispatch(therePriceAction(mockFlightInfo[activeRoute].price));
        } else if (direction === 'back') {
            dispatch(backDepartureTimeAction(mockFlightInfo[activeRoute].departureTime));
            dispatch(backArrivalTimeAction(mockFlightInfo[activeRoute].arrivalTime));
            dispatch(backPriceAction(mockFlightInfo[activeRoute].price));
        }
    })
    
    return (
        <div className="oneway-card">
            <LogoPart logo={S7}/>
            <div className="oneway-card__flight__wrapper">
                <FlightRoute object={mockFlightInfo[activeRoute]}/>
                <div className="oneway-card__flight__variants">
                    {mockFlightInfo.map((item, index)=> {
                        return <FlightVariant 
                                    className={index === activeRoute ? 'active' : 'non-active'} 
                                    departureTime={item.departureTime} 
                                    arrivalTime={item.arrivalTime}
                                    handleFunc = {()=>setActiveRoute(index)}
                                    key={index}
                                />
                    })}
                </div>
            </div>
            <div className="oneway-card__price__wrapper">
                <p>{mockFlightInfo[activeRoute].price} р.</p>
            </div>
        </div>
    )
}

export default OneWayCard;