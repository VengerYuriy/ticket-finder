import './style.scss'

import S7 from '../../../../assets/icons/S7.svg'
import LogoPart from '../LogoPart'
import FlightRoute from '../FlightRoute'
import { selectArrivalDirection, selectThereDate, selectDepartureDirection, selectArrivalAirport, selectDepartureAirport, selectBackDate, selectThereDepartureTime, selectThereArrivalTime, selectTherePrice, selectBackDepartureTime, selectBackArrivalTime, selectBackPrice } from '../../../../store/search/selectors'
import { useSelector } from 'react-redux'



const RoundTripCard = () => {
    const departureDirection = useSelector(selectDepartureDirection),
          arrivalDirection = useSelector(selectArrivalDirection),
          thereDate = useSelector(selectThereDate),
          backDate = useSelector(selectBackDate),
          arrivalAirport = useSelector(selectArrivalAirport),
          departureAirport = useSelector(selectDepartureAirport),
          thereDepartureTime = useSelector(selectThereDepartureTime),
          thereArrivalTime = useSelector(selectThereArrivalTime),
          therePrice = useSelector(selectTherePrice),
          backDepartureTime = useSelector(selectBackDepartureTime),
          backArrivalTime = useSelector(selectBackArrivalTime),
          backPrice = useSelector(selectBackPrice);
          
    const thereRoute = {
        departureCity: departureDirection,
        departureAirport: departureAirport,
        arrivalCity: arrivalDirection,
        arrivalAirport: arrivalAirport,
        departureDate: thereDate,
        arrivalDate: thereDate,
        departureTime: thereDepartureTime,
        arrivalTime: thereArrivalTime,
        price: therePrice,
        company: 'S7'
    }

    const backRoute = {
        departureCity: arrivalDirection,
        departureAirport: arrivalAirport,
        arrivalCity: departureDirection,
        arrivalAirport: departureAirport,
        departureDate: backDate,
        arrivalDate: backDate,
        departureTime: backDepartureTime,
        arrivalTime: backArrivalTime,
        price: backPrice,
        company: 'S7'
    }
    
    return (
        <div className="roundtrip__wrapper">
            <div>
                <div className="roundtrip-card">
                    <LogoPart logo={S7}/>
                    <div className="roundtrip-card__flight__wrapper">
                        <FlightRoute object={thereRoute}/>
                    </div>
                </div>
                <div className="roundtrip-card">
                    <LogoPart logo={S7} />
                    <div className="roundtrip-card__flight__wrapper back">
                        <FlightRoute object={backRoute}/>
                    </div>
                </div>
            </div>
            <div className="roundtrip-card__price__wrapper">
                <p>{backPrice+therePrice} р.</p>
            </div>
        </div>
    )
}

export default RoundTripCard;