import './style.scss'
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import OneWayCard from "../../components/infoCard/components/OneWayCard";
import RoundTripCard from "../../components/infoCard/components/RoundTripCard";
import { selectRoute } from "../../store/search/selectors";

const InfoPage = () => {

    const route = useSelector(selectRoute)

    return (
        <div className="info-page">
            <Link className={`back-button`} to={'/avia'}> <p className="back-button__text">К поиску</p> </Link>
            {route === 'one-way' && <OneWayCard direction="there"/>}
            {route === 'round-trip' && <><RoundTripCard/><OneWayCard direction="there"/> <OneWayCard direction="back"/></>}
        </div>
    )
}

export default InfoPage;