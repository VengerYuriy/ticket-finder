
import SearchButton from '../../components/SearchButton';
import './style.scss'
import "react-datepicker/dist/react-datepicker.css";
import DirectionField from '../../components/fields/DirectionField';
import DateField from '../../components/fields/DateField';
import { backDateAction, arrivalDirectionAction, thereDateAction, departureDirectionAction, arrivalAirportAction, departureAirportAction, clearStateAction } from '../../store/search/actions';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';



const SearchPage = ({src}:{src: string }) => {
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(clearStateAction())
    },[dispatch])
    
    return (
        <div className="avia__wrapper">
            <div className="search__wrapper">
                <div className="search__fields__wrapper">
                    <DirectionField title='Откуда' placeholder='Город вылета' actionCity={departureDirectionAction} actionAirport = {departureAirportAction}/>
                    <DirectionField title='Куда' placeholder='Город прилета' actionCity={arrivalDirectionAction} actionAirport = {arrivalAirportAction}/>
                    <DateField title='Туда' placeholder='дд.мм.гг' action={thereDateAction}/>
                    <DateField title='Обратно' placeholder='дд.мм.гг' action={backDateAction}/>
                </div>
                <div className="search__button__wrapper">
                    <SearchButton linkSrc={src}/>
                </div>
            </div>
        </div>
    )
}

export default SearchPage;