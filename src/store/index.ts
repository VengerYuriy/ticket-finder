import { combineReducers, legacy_createStore as createStore } from "redux";
import searchReducer from "./search/reducers";

const rootReducer = combineReducers({searchReducer})

const store = createStore (rootReducer)

export default store;