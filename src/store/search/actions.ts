
export const departureDirectionAction = (value: string) => {
    return {
        type: 'search/departureDirection',
        payload: value 
    } 
}
export const departureAirportAction = (value: string) => {
    return {
        type: 'search/departureAirport',
        payload: value 
    } 
}
export const arrivalDirectionAction = (value: string) => {
    return {
        type: 'search/arrivalDirection',
        payload: value 
    } 
}
export const arrivalAirportAction = (value: string) => {
    return {
        type: 'search/arrivalAirport',
        payload: value 
    } 
}
export const thereDateAction = (value: string) => {
    return {
        type: 'search/thereDate',
        payload: value 
    } 
}
export const thereDepartureTimeAction = (value: string) => {
    return {
        type: 'search/thereDepartureTime',
        payload: value 
    } 
}
export const thereArrivalTimeAction = (value: string) => {
    return {
        type: 'search/thereArrivalTime',
        payload: value 
    } 
}
export const therePriceAction = (value: number) => {
    return {
        type: 'search/therePrice',
        payload: value 
    } 
}
export const backDateAction = (value: string) => {
    return {
        type: 'search/backDate',
        payload: value 
    } 
}
export const backDepartureTimeAction = (value: string) => {
    return {
        type: 'search/backDepartureTime',
        payload: value 
    } 
}
export const backArrivalTimeAction = (value: string) => {
    return {
        type: 'search/backArrivalTime',
        payload: value 
    } 
}
export const backPriceAction = (value: number) => {
    return {
        type: 'search/backPrice',
        payload: value 
    } 
}

export const routeAction = (value: string) => {
    return {
        type: 'search/route',
        payload: value 
    } 
}

export const clearStateAction = () => {
    return {
        type: 'search/clearState',
    } 
}