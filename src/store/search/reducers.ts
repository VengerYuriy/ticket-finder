import { ISearchAction } from "./types";

const initalState = {
    departureDirection: '',
    departureAirport: '',
    arrivalDirection: '',
    arrivalAirport: '',
    thereDate: '',
    thereDepartureTime:'',
    thereArrivalTime:'',
    therePrice: 0,
    backDate: '',
    backDepartureTime:'',
    backArrivalTime:'',
    backPrice: 0,
    route: 'none',
}

const searchReducer = (state = initalState, action: ISearchAction) => {
    switch (action.type) {
        case 'search/departureDirection':
            return {...state, departureDirection: action.payload};
        case 'search/departureAirport':
            return {...state, departureAirport: action.payload};
        case 'search/arrivalDirection':
            return {...state, arrivalDirection: action.payload};
        case 'search/arrivalAirport':
            return {...state, arrivalAirport: action.payload};
        case 'search/thereDate':
            return {...state, thereDate: action.payload};
        case 'search/thereDepartureTime':
            return {...state, thereDepartureTime: action.payload};
        case 'search/thereArrivalTime':
            return {...state, thereArrivalTime: action.payload};
        case 'search/therePrice':
            return {...state, therePrice: action.payload};
        case 'search/backDate':
            return {...state, backDate: action.payload};
        case 'search/backDepartureTime':
            return {...state, backDepartureTime: action.payload};
        case 'search/backArrivalTime':
            return {...state, backArrivalTime: action.payload};
        case 'search/backPrice':
            return {...state, backPrice: action.payload};
        case 'search/route':
            return {...state, route: action.payload};
        case 'search/clearState':
            return initalState
        default:
            return state;
    }
}

export default searchReducer;