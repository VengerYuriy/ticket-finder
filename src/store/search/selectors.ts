import { IStore } from "./types"

export const selectDepartureDirection = (state: IStore) => state.searchReducer.departureDirection
export const selectArrivalDirection = (state: IStore) => state.searchReducer.arrivalDirection
export const selectDepartureAirport = (state: IStore) => state.searchReducer.departureAirport
export const selectArrivalAirport = (state: IStore) => state.searchReducer.arrivalAirport
export const selectThereDate = (state: IStore) => state.searchReducer.thereDate
export const selectThereDepartureTime = (state: IStore) => state.searchReducer.thereDepartureTime
export const selectThereArrivalTime = (state: IStore) => state.searchReducer.thereArrivalTime
export const selectTherePrice = (state: IStore) => state.searchReducer.therePrice
export const selectBackDate = (state: IStore) => state.searchReducer.backDate
export const selectBackDepartureTime = (state: IStore) => state.searchReducer.backDepartureTime
export const selectBackArrivalTime = (state: IStore) => state.searchReducer.backArrivalTime
export const selectBackPrice = (state: IStore) => state.searchReducer.backPrice
export const selectRoute = (state: IStore) => state.searchReducer.route