export interface IStore {
    searchReducer: ISearchReducer
}

export interface ISearchAction {
    type: string,
    payload: string
}

export interface ISearchReducer {
    departureDirection: string,
    arrivalDirection: string,
    departureAirport: string,
    arrivalAirport: string,
    thereDate: string,
    thereDepartureTime: string,
    thereArrivalTime: string,
    therePrice: number,
    backDate: string,
    backDepartureTime: string,
    backArrivalTime: string,
    backPrice: number,
    route: string
}